FROM phusion/baseimage:0.9.15
MAINTAINER smdion <me@seandion.com>

ENV DEBIAN_FRONTEND noninteractive

# Set correct environment variables
ENV HOME /root

# Set the locale
RUN locale-gen en_US.UTF-8  
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8 

# Use baseimage-docker's init system
CMD ["/sbin/my_init"]

# Fix a Debianism of the nobody's uid being 65534
RUN usermod -u 99 nobody
RUN usermod -g 100 nobody
RUN usermod -d /config nobody

# Add default config
ADD config.yml /config.yml

#setup config volume
VOLUME /config

#install deluge and flexget
RUN add-apt-repository "deb http://us.archive.ubuntu.com/ubuntu/ trusty universe multiverse"
RUN add-apt-repository "deb http://us.archive.ubuntu.com/ubuntu/ trusty-updates universe multiverse"
RUN apt-get update -qq
RUN apt-get install -qq --assume-yes python2.7 python-dev python-pip python-transmissionrpc wget
RUN apt-get install -qq --assume-yes deluge
RUN apt-get autoremove 
RUN apt-get autoclean
RUN pip install flexget
RUN pip install --upgrade six>=1.70
RUN ln -sf /config /root/.flexget
    
# Add firstrun.sh to runit
ADD firstrun.sh /etc/my_init.d/firstrun.sh
RUN chmod +x /etc/my_init.d/firstrun.sh

# Add flexget to runit
RUN mkdir /etc/service/flexget
ADD flexget.sh /etc/service/flexget/run
RUN chmod +x /etc/service/flexget/run
